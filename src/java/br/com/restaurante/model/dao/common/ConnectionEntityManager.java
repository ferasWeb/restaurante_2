/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.restaurante.model.dao.common;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Andressa
 */
public class ConnectionEntityManager {

    
    private ConnectionEntityManager(){
        
    }
    
    public static EntityManager getConnection() {
        EntityManagerFactory fac = Persistence.createEntityManagerFactory("restaurantePU");
        EntityManager em = fac.createEntityManager();
        return em;
    }
}
