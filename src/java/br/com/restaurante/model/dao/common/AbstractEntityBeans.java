/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.restaurante.model.dao.common;

import br.com.restaurante.model.entity.common.EntityInterface;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

/**
 *
 * @author arthur.diego
 */
public abstract class AbstractEntityBeans<T extends EntityInterface, ID extends Serializable> {

    public EntityManager em = ConnectionEntityManager.getConnection();

    private final Class<T> entityClass;

    public AbstractEntityBeans(final Class<T> entityClass) {
        this.entityClass = entityClass;

    }

    public void save(T entity) {
        try {
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();

        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {

            em.close();
        }
    }

    public void update(T entity) {
        try {
            em.getTransaction().begin();
            em.merge(entity);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public void delete(T entity) {
        try {
            em.getTransaction().begin();
            em.remove(em.merge(entity));
            em.flush();
        } catch (Exception e) {
        }
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq
                = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return em.createQuery(cq).getResultList();
    }

    public T find(final ID id) {
        try {
            return em.find(entityClass, id);
        } catch (NoResultException e) {
            return null;
        }
    }
}
