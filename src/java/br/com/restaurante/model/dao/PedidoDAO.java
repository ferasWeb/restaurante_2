/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.restaurante.model.dao;

import br.com.restaurante.model.dao.common.AbstractEntityBeans;
import br.com.restaurante.model.entity.Pedido;

/**
 *
 * @author Andressa
 */
public class PedidoDAO extends AbstractEntityBeans<Pedido, Long> {

    public PedidoDAO() {
        super(Pedido.class);
    }
    
}
