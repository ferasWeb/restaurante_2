/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.restaurante.model.entity;

import br.com.restaurante.model.entity.common.EntityInterface;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Aluno
 */
@Entity
@Table(name = "tbl_garcon")
public class Garcon implements EntityInterface{

    @Id
    @GeneratedValue
    private Long idGarcon;

    @Column(name = "nome", nullable = false)
    private String nome;
    
    @Column (name = "senha", nullable = false)
    private String senha;

    public Long getIdGarcon() {
        return idGarcon;
    }

    public void setIdGarcon(Long idGarcon) {
        this.idGarcon = idGarcon;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    
  

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
