/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.restaurante.model.entity.common;

import java.io.Serializable;

/**
 *
 * @author Andressa
 */
public interface EntityInterface  <T> extends Comparable<T>, Serializable {
    
}
